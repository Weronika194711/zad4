#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH

#include <iostream>
#include <iomanip>
#include <cmath>
#include "Macierz.hh"
#include "Wektor.hh"

//using namespace std;

template <typename T, int ROZMIAR> 
class UkladRownanLiniowych {          

    Wektor<T, ROZMIAR> WektorRozwiazan; //wektor rozwiazan
    Wektor<T, ROZMIAR> WektorBledu; //wektor bledu
    T DlugoscWektoraBledu; //dlugosc wektora
    Wektor<T, ROZMIAR> WektorWyrazowWolnych; //wektor wyrazow wolnych
    Wektor<T, ROZMIAR> WektorWyznacznikow; //wektor zawierajacy wyznaczniki macierzy z zamienionymi kolumnami
    Macierz<T, ROZMIAR> MacierzWspolczynnikow; // macierz
    T WyznacznikGlownyMacierzy; //wyznacznik glowny

/*!
 * Realizuje zamiane kolumn potrzebna przy rozwiazywaniu ukladu rownan
 * metoda Cramera
 * Argumenty:
 * j- nr kolumny
 */
    void ZamienKolumnyCramer(int j); 
/*!
 * Realizuje wyliczanie wyznacznikow macierzy z zamienionymi kolumnami
 */
    void ObliczWyznaczniki(); 

/*!
 * Realizuje wyliczenie dlugosci wektora
 */
    void ObliczDlugoscWektoraBledu();                                                                                                                                        

 public:                                                                                                                                                                      

/*!
 * Realizuje rozwiazywanie ukladu rownan za pomoca wzorow Cramera
 * Zwraca:
 * 0 -jesli wyznacznik glowny jest rozny od 0
 * 1- uklad rownan jest sprzeczny
 * 2- uklad rownan ma nieskonczenie wiele rozwiazan
 */
    int ObliczWzoramiCramera();
/*!
 * Realizuje wyliczenie wektora bledu i jego dlugosci
 */
    void ObliczWektorBledow();

/*!
 * Realizuje wyswietlenie odpowiedzi
 */
     void WyswietlWyniki();
/*!
 * Realizuje przeciazanie operatora dla wyswietlania ukladu rownan
 * Argumenty:
 *    Strm- strumien wejscia
 *    UklRown -uklad rownan, ktory funkcja ma wczytac,
 *Zwraca:
 *      Referencje do obiektu istream
 */                                                                                                                                                                     
    friend std::istream& operator >> (std::istream &Strm, UkladRownanLiniowych<T, ROZMIAR> &UklRown){   //wyswietlanie
    Strm >> UklRown.MacierzWspolczynnikow;
    Strm >> UklRown.WektorWyrazowWolnych;
    return Strm;
}

/*!
 * Realizuje przeciazanie operatora dla wyswietlania ukladu rownan
 * Argumenty:
 *    Strm- strumien wyjscia
 *    LinEq -uklad rownan, ktory funkcja ma wyswietlic,
 *Zwraca:
 *      Referencje do obiektu ostream
 */
    friend std::ostream& operator << ( std::ostream &Strm, const UkladRownanLiniowych<T, ROZMIAR> &UklRown){     //wczytywanie
    for (int i = 0; i < ROZMIAR; i++) {
        Strm << "| ";
        for (int j = 0; j < ROZMIAR; j++) {
            Strm << std::setw(3) << UklRown.MacierzWspolczynnikow(j,i) << " ";
        }
        Strm << "| |";
        Strm << "x" << i+1;

        Strm << "| = | ";
        Strm << std::setw(3) << UklRown.WektorWyrazowWolnych[i] << " | " << endl;
    }
    return Strm;
    }                                                                                   
};

template <typename T, int ROZMIAR> 
void UkladRownanLiniowych<T, ROZMIAR>::ZamienKolumnyCramer(int j) {     //zamiana kolumn
    for (int i = 0; i < ROZMIAR; i++) {
        std::swap(MacierzWspolczynnikow(i,j), WektorWyrazowWolnych[i]);
    }
}

template <typename T, int ROZMIAR> 
void UkladRownanLiniowych<T, ROZMIAR>::ObliczWyznaczniki() {    //wyliczenie wyznacznikow macierzy z zamienionymi kolumnami

    for (int j = 0; j < ROZMIAR; j++) {
        ZamienKolumnyCramer(j); //zamiana kolumn
        WektorWyznacznikow[j] = MacierzWspolczynnikow.WyznacznikMacierzy(MacierzWspolczynnikow); //wyliczenie wyznacznika
        ZamienKolumnyCramer(j); //powrot do pierwotnej macierzy
    }
}

template <typename T, int ROZMIAR> 
int UkladRownanLiniowych<T, ROZMIAR>::ObliczWzoramiCramera() {   //rozwiazanie ukladu rownan wzorami cramera

    MacierzWspolczynnikow.Transponowanie(); //transpozycja macierzy
    std::cout<<"Macierz A^T"<<endl;
    std::cout<<MacierzWspolczynnikow<<endl;
    WyznacznikGlownyMacierzy =MacierzWspolczynnikow.WyznacznikMacierzy(MacierzWspolczynnikow); //wyliczenie glownego wyznacznik
    std::cout<<"Macierz wyznacznikow:"<<endl;
    std::cout<<MacierzWspolczynnikow<<endl;
    ObliczWyznaczniki(); //wyliczenie wyznacznikow macierzy z zamienionymi kolumnami
    if (WyznacznikGlownyMacierzy != 0) {
        WektorRozwiazan = WektorWyznacznikow / WyznacznikGlownyMacierzy;
        return 0;
    }
    else {
        for (int i = 0; i < ROZMIAR; i++) {
            if (WektorWyznacznikow[i] != 0)
                return 1;
        }
        return 2;
    }
}

template <typename T, int ROZMIAR> 
void UkladRownanLiniowych<T, ROZMIAR>:: WyswietlWyniki(){   // wyswietlenie odpowiedzi
    switch (ObliczWzoramiCramera()) {
        case 0: {
            std::cout<<endl;
            std::cout<< "Rozwiazanie x = ( ";
            for (int i = 0; i < ROZMIAR; i++) {
                std::cout << "x"<<i+1;
                if (i < (ROZMIAR-1)) {
                    std::cout << ", ";
                }
            }
            std::cout<<"):"<<endl;

            std::cout <<"\t"<<WektorRozwiazan << endl;
            std::cout << endl;
            break;
        }
        case 1:{
            std::cout<<"Uklad sprzeczny - brak rozwiazania"<<endl;
            break;
        }
        case 2:{
            std::cout<<"Uklad nieoznaczony - nieskonczenie wiele rozwiazan"<<endl;
            break;
        }
    }

}
#endif
