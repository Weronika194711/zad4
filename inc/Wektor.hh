#ifndef WEKTOR_HH
#define WEKTOR_HH

#include "rozmiar.hh"
#include "LZespolona.hh"
#include <iostream>

using namespace std;

template <typename T, int ROZMIAR>
class Wektor
{

    T wektor[ROZMIAR];

public:
    /*!
 * Przeciazenia operatora indeksujacego dla wektorow
 * Argumenty:
 * indeks -rozmiar wektora
 * Zwraca:
 * Wektor o danym rozmiarze
 */
    T &operator[](unsigned int indeks);

    /*!
 * Przeciazenia operatora indeksujacego dla wektorow
 * Argumenty:
 * indeks -rozmiar wektora
 * Zwraca:
 * Wektor o danym rozmiarze
 * lub w przypadku zlego rozmiaru komunikat o bledzie
 */
    const T &operator[](unsigned int indeks) const;

    /*!                                                                                                                                                                              
 * Mnozenie wektora przez wektor-iloczyn skalarny                                                                                                                                
 * Argumenty:                                                                                                                                                                    
 * wektor2- wektor, przez ktory mamy pomnozyc inny wektor                                                                                                                        
 * Zwraca:                                                                                                                                                                       
 * iloczyn skalarny wektorow                                                                                                                                                     
 */
    T operator*(Wektor<T, ROZMIAR> wektor2) const;

    /*!
 * Mnozenie wektora przez liczbe
 * Argumenty
 * a- liczba, przez ktora mamy pomnozyc wektor
 * Zwraca:
 * Wynik mnozenia
 */
    Wektor<T, ROZMIAR> operator*(T a) const;

    /*!                                                                                                                                                                              
 * Dzielenie wektora przez liczbe                                                                                                                                                
 * Argumenty:                                                                                                                                                                    
 * a- liczba, przez ktora mamy podzielic wektor                                                                                                                                  
 * Zwraca:                                                                                                                                                                       
 * Wynik dzielenia                                                                                                                                                               
 */
    Wektor<T, ROZMIAR> operator/(T a) const;

    /*!                                                                                                                                                                              
 * Dodawanie wektorow                                                                                                                                                            
 * Argumenty:                                                                                                                                                                    
 * wektor2- wektor, ktory mamy dodac do innego wektora                                                                                                                           
 * Zwraca:                                                                                                                                                                       
 * sume wektorow                                                                                                                                                                 
 */
    Wektor<T, ROZMIAR> operator+(Wektor<T, ROZMIAR> wektor2) const;

    /*!                                                                                                                                                                              
 * Odejmowanie wektorow                                                                                                                                                          
 * Argumenty:                                                                                                                                                                    
 * wektor2- wektor, ktory mamy odjac od innego wektora                                                                                                                           
 * Zwraca:                                                                                                                                                                       
 * roznice wektorow                                                                                                                                                              
 */
    Wektor<T, ROZMIAR> operator-(Wektor<T, ROZMIAR> wektor2) const;

    /*!                                                                                                                                                                              
 * Konstruktor dla klasy Wektor                                                                                                                                                  
 * Inicjuje pola klasy poczatkowymi wartosciami                                                                                                                                  
 */
    Wektor();
    
};


/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wczytywania wektora                                                                                                                      
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wejscia                                                                                                                                                     
 *    Wek - wektor, ktory funkcja ma wczytac,                                                                                                                                    
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu istream                                                                                                                                            
 */
template <typename T, int ROZMIAR>
std::istream &operator>>(std::istream &Strm, Wektor<T, ROZMIAR> &Wek);

/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wyswietlania wektora                                                                                                                     
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wyjscia                                                                                                                                                     
 *    Wek - wektor, ktory funkcja ma wyswietlic,                                                                                                                                 
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu ostream                                                                                                                                            
 */
template <typename T, int ROZMIAR>
std::ostream &operator<<(std::ostream &Strm, const Wektor<T, ROZMIAR> &Wek);

template <typename T, int ROZMIAR>
Wektor<T, ROZMIAR>::Wektor()
{ //konstruktor
    for (int i = 0; i < ROZMIAR; i++)
    {
        wektor[i] = 0;
    }
}

template <typename T, int ROZMIAR>
const T &Wektor<T, ROZMIAR>::operator[](unsigned int indeks) const
{ //operator indeksujacy
    if (indeks < 0 || indeks >= ROZMIAR)
    {
        clog << "Indeks tablicy wyszedl poza zakres" << endl;
        exit(1);
    }
    return wektor[indeks];
}

template <typename T, int ROZMIAR>
T &Wektor<T, ROZMIAR>::operator[](unsigned int indeks)
{ //operator indeksujacy

    return const_cast<T &>(const_cast<const Wektor<T, ROZMIAR> *>(this)->operator[](indeks));
}

template <typename T, int ROZMIAR>
istream &operator>>(std::istream &Strm, Wektor<T, ROZMIAR> &Wek)
{ // wczytywanie wektora
    for (int i = 0; i < ROZMIAR; i++)
    {
        Strm >> Wek[i];
    }
    return Strm;
}

template <typename T, int ROZMIAR>
ostream &operator<<(std::ostream &Strm, const Wektor<T, ROZMIAR> &Wek)
{ // wyswietlanie wektora
    Strm << "(";
    for (int i = 0; i < ROZMIAR; i++)
    {
        Strm << Wek[i] << " ";
        if (i < (ROZMIAR - 1))
        {
            Strm << ", ";
        }
    }
    Strm << ")";
    return Strm;
}

template <typename T, int ROZMIAR>
T Wektor<T, ROZMIAR>::operator*(Wektor<T, ROZMIAR> wektor2) const
{ //iloczyn skalarny
    T iloczyn_skalarny = 0;
    for (int i = 0; i < ROZMIAR; i++)
    {
        iloczyn_skalarny = iloczyn_skalarny + wektor[i] * wektor2[i];
    }
    return iloczyn_skalarny;
}

template <typename T, int ROZMIAR>
Wektor<T, ROZMIAR> Wektor<T, ROZMIAR>::operator*(T a) const
{ //mnozenie przez liczbe
    Wektor<T, ROZMIAR> nowy_wektor{};
    for (int i = 0; i < ROZMIAR; i++)
    {
        nowy_wektor[i] = wektor[i] * a;
    }
    return nowy_wektor;
}

template <typename T, int ROZMIAR>
Wektor<T, ROZMIAR> Wektor<T, ROZMIAR>::operator/(T a) const
{ //dzielenie przez liczbe
    Wektor<T, ROZMIAR> nowy_wektor{};
    for (int i = 0; i < ROZMIAR; i++)
    {
        nowy_wektor[i] = wektor[i] / a;
    }
    return nowy_wektor;
}

template <typename T, int ROZMIAR>
Wektor<T, ROZMIAR> Wektor<T, ROZMIAR>::operator+(Wektor<T, ROZMIAR> wektor2) const
{ //dodawanie wektorow
    Wektor<T, ROZMIAR> nowy_wektor{};
    for (int i = 0; i < ROZMIAR; i++)
    {
        nowy_wektor[i] = wektor[i] + wektor2[i];
    }
    return nowy_wektor;
}

template <typename T, int ROZMIAR>
Wektor<T, ROZMIAR> Wektor<T, ROZMIAR>::operator-(Wektor<T, ROZMIAR> wektor2) const
{ //odejmowanie wektorow
    Wektor<T, ROZMIAR> nowy_wektor{};
    for (int i = 0; i < ROZMIAR; i++)
    {
        nowy_wektor[i] = wektor[i] - wektor2[i];
    }
    return nowy_wektor;
}

#endif
