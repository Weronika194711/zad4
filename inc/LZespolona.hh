#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include <iostream>
#include <cmath>
#include <climits>
#include <cstring>
#include <cassert>

using namespace std;

/*!                                                                                                                                                                              
 *  Plik zawiera definicje struktury LZesplona oraz zapowiedzi                                                                                                                   
 *  przeciazen operatorow arytmetycznych dzialajacych na tej                                                                                                                     
 *  strukturze.                                                                                                                                                                  
 */


/*!                                                                                                                                                                              
 * Modeluje pojecie liczby zespolonej                                                                                                                                            
 */
struct  LZespolona {
  double   re;    /*! Pole repezentuje czesc rzeczywista. */
  double   im;    /*! Pole repezentuje czesc urojona. */
  LZespolona &operator = (double  liczba); //operator przypisania

};

LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator - (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator * (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona operator * (LZespolona Skl1, double liczba); //mnozenia liczby zespolonej przez liczbe rzeczywista
LZespolona  operator % (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator / (LZespolona  Skl1,  LZespolona  Skl2); // dzielenie przez liczbe zespolona                                                                                
LZespolona  operator / (LZespolona  Skl1,  double k); // dzielenie przez liczbe calkowita                                                                                        
LZespolona Sprzezenie (LZespolona Skl2); // sprzezenie liczby zespolonej                                                                                                         
double Modul2 (LZespolona Skl2); // wyliczenie modulu                                                                                                                            
std:: ostream& operator<<(std:: ostream &StrmWy, LZespolona k);
std:: istream& operator>>(std:: istream &StrmWe, LZespolona &k);                                                                            
#endif
