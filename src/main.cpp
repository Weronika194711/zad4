#include <iostream>
#include "UkladRownanLiniowych.hh"

using namespace std;

void Test_SzablonMacierz_double()
{
  cout << endl
       << " --------- Test klasy Macierz<double," << ROZ << "> ----------" << endl
       << endl;

  Wektor<double,ROZ> W, W_wynik;

  for (unsigned int Ind = 0; Ind < ROZ; ++Ind) {
    W[Ind] = Ind;
  }

  cout << "  Wyswietlenie wspolrzednych macierzy: W" << endl
       << "        " << W << endl
       << endl;

  W_wynik = W*2;
  cout << "  Wyswietlenie wyniku: W*2" << endl
       << "        " << W_wynik << endl
       << endl;

  W_wynik = W*2-W;
  cout << "  Wyswietlenie wyniku: W-W*0.5" << endl
       << "        " << W_wynik << endl
       << endl;
}

void Test_SzablonMacierz_LZespolona()
{
  cout << endl
       << " --------- Test klasy Macierz<LZespolona," << ROZ << "> ----------" << endl
       << endl;


Wektor<LZespolona,ROZ>    W, W_wynik;
LZespolona LZ;
LZ.re=2;LZ.im=2;
  for (unsigned int Ind = 0; Ind < ROZ; ++Ind) {
    W[Ind].re = Ind;      W[Ind].im = Ind;
  }

  cout << "  Wyswietlenie wspolrzednych macierzy: W" << endl
       << "        " << W << endl
       << endl;

  W_wynik = W*LZ;
  cout << "  Wyswietlenie wyniku: W*2" << endl
       << "        " << W_wynik << endl
       << endl;

  W_wynik = W*LZ-W;
  cout << "  Wyswietlenie wyniku: W-W*0.5" << endl
       << "        " << W_wynik << endl
       << endl;
}

int main() {
    Test_SzablonMacierz_double();
    Test_SzablonMacierz_LZespolona();
}