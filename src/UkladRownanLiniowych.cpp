#include "UkladRownanLiniowych.hh"
#include "Wektor.hh"
#include "Macierz.hh"
#include "rozmiar.hh"

using namespace std;

/*!
 * Realizuje przeciazanie operatora dla wyswietlania ukladu rownan
 * Argumenty:
 *    Strm- strumien wejscia
 *    UklRown -uklad rownan, ktory funkcja ma wczytac,
 *Zwraca:
 *      Referencje do obiektu istream
 */
istream& operator >> (istream &Strm, UkladRownanLiniowych &UklRown) {
    Strm >> UklRown.MacierzWspolczynnikow;
    Strm >> UklRown.WektorWyrazowWolnych;
    return Strm;
}

/*!
 * Realizuje przeciazanie operatora dla wyswietlania ukladu rownan
 * Argumenty:
 *    Strm- strumien wyjscia
 *    LinEq -uklad rownan, ktory funkcja ma wyswietlic,
 *Zwraca:
 *      Referencje do obiektu ostream
 */
ostream& operator << (ostream &Strm, const UkladRownanLiniowych &UklRown) {
    for (int i = 0; i < ROZMIAR; i++) {
        Strm << "| ";
        for (int j = 0; j < ROZMIAR; j++) {
            Strm << setw(3) << UklRown.MacierzWspolczynnikow(j, i) << " ";
        }
        Strm << "| |";
        Strm << "x" << i+1;

        Strm << "| = | ";
        Strm << setw(3) << UklRown.WektorWyrazowWolnych[i] << " | " << endl;
    }
    return Strm;
}

/*!
 * Realizuje zamiane kolumn potrzebna przy rozwiazywaniu ukladu rownan
 * metoda Cramera
 * Argumenty:
 * j- nr kolumny
 */
void UkladRownanLiniowych::ZamienKolumnyCramer(int j) {
    for (int i = 0; i < ROZMIAR; i++) {
        swap(MacierzWspolczynnikow(i,j), WektorWyrazowWolnych[i]);
    }
}

/*!
 * Realizuje wyliczanie wyznacznikow macierzy z zamienionymi kolumnami
 */
void UkladRownanLiniowych::ObliczWyznaczniki() {

    for (int j = 0; j < ROZMIAR; j++) {
        ZamienKolumnyCramer(j); //zamiana kolumn
        WektorWyznacznikow[j] = MacierzWspolczynnikow.WyznacznikMacierzy(MacierzWspolczynnikow); //wyliczenie wyznacznika
        ZamienKolumnyCramer(j); //powrot do pierwotnej macierzy
    }
}

/*!
 * Realizuje rozwiazywanie ukladu rownan za pomoca wzorow Cramera
 * Zwraca:
 * 0 -jesli wyznacznik glowny jest rozny od 0
 * 1- uklad rownan jest sprzeczny
 * 2- uklad rownan ma nieskonczenie wiele rozwiazan
 */
int UkladRownanLiniowych::ObliczWzoramiCramera() {

    MacierzWspolczynnikow.Transponowanie(); //transpozycja macierzy
    cout<<"Macierz A^T"<<endl;
    cout<<MacierzWspolczynnikow<<endl;
    WyznacznikGlownyMacierzy =MacierzWspolczynnikow.WyznacznikMacierzy(MacierzWspolczynnikow); //wyliczenie glownego wyznacznik
    cout<<"Macierz wyznacznikow:"<<endl;
    cout<<MacierzWspolczynnikow<<endl;
    ObliczWyznaczniki(); //wyliczenie wyznacznikow macierzy z zamienionymi kolumnami
    if (WyznacznikGlownyMacierzy != 0) {
        WektorRozwiazan = WektorWyznacznikow / WyznacznikGlownyMacierzy;
        return 0;
    }
    else {
        for (int i = 0; i < ROZMIAR; i++) {
            if (WektorWyznacznikow[i] != 0)
                return 1;
        }
        return 2;
    }
}

/*!
 * Realizuje wyliczenie dlugosci wektora
 */
void UkladRownanLiniowych::ObliczDlugoscWektoraBledu() {
    DlugoscWektoraBledu = sqrt(WektorBledu*WektorBledu);
}

/*!
 * Realizuje wyliczenie wektora bledu i jego dlugosci
 */
void UkladRownanLiniowych::ObliczWektorBledow() {
    WektorBledu = MacierzWspolczynnikow * WektorRozwiazan - WektorWyrazowWolnych;
    ObliczDlugoscWektoraBledu();
}

/*!
 * Realizuje wyswietlenie odpowiedzi
 */
void UkladRownanLiniowych:: WyswietlWyniki(){
    switch (ObliczWzoramiCramera()) {
        case 0: {
            cout<<endl;
            cout<< "Rozwiazanie x = ( ";
            for (int i = 0; i < ROZMIAR; i++) {
                cout << "x"<<i+1;
                if (i < (ROZMIAR-1)) {
                    cout << ", ";
                }
            }
            cout<<"):"<<endl;

            cout <<"\t"<<WektorRozwiazan << endl;
            cout << endl;

            cout << "\t \t Wektor bledu:  Ax-b: ";
            cout << WektorBledu << endl;

            cout << " Dlugosc wektora bledu: ||Ax-b|| = ";
            cout << DlugoscWektoraBledu << endl;
            cout << endl;

            ObliczWektorBledow();
            break;
        }
        case 1:{
            cout<<"Uklad sprzeczny - brak rozwiazania"<<endl;
            break;
        }
        case 2:{
            cout<<"Uklad nieoznaczony - nieskonczenie wiele rozwiazan"<<endl;
            break;
        }
    }

}