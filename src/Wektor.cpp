#include "UkladRownanLiniowych.hh"
#include "Wektor.hh"
#include "Macierz.hh"
#include "rozmiar.hh"

using namespace std;


/*!                                                                                                                                                                              
 * Konstruktor dla klasy Wektor                                                                                                                                                  
 * Inicjuje pola klasy poczatkowymi wartosciami                                                                                                                                  
 */
Wektor::Wektor(){
    for(int i=0; i<ROZMIAR; i++)
    {
        wektor[i] = 0;
    }
}

/*!
 * Przeciazenia operatora indeksujacego dla wektorow
 * Argumenty:
 * indeks -rozmiar wektora
 * Zwraca:
 * Wektor o danym rozmiarze
 * lub w przypadku zlego rozmiaru komunikat o bledzie
 */
const double& Wektor:: operator[](unsigned int indeks) const {
    if (indeks<0 || indeks>=ROZMIAR) {
        clog << "Indeks tablicy wyszedl poza zakres" << endl;
        exit(1);
    }
    return wektor[indeks];
}

/*!
 * Przeciazenia operatora indeksujacego dla wektorow
 * Argumenty:
 * indeks -rozmiar wektora
 * Zwraca:
 * Wektor o danym rozmiarze
 */
double& Wektor:: operator[](unsigned int indeks) {

    return const_cast<double &>(const_cast<const Wektor *>(this)->operator[](indeks));
}

/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wczytywania wektora                                                                                                                      
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wejscia                                                                                                                                                     
 *    Wek - wektor, ktory funkcja ma wczytac,                                                                                                                                    
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu istream                                                                                                                                            
 */
istream& operator >> (std::istream &Strm, Wektor &Wek){
    for(int i=0; i<ROZMIAR; i++)
    {
        Strm>>Wek[i];
    }
    return Strm;
}

/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wyswietlania wektora                                                                                                                     
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wyjscia                                                                                                                                                     
 *    Wek - wektor, ktory funkcja ma wyswietlic,                                                                                                                                 
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu ostream                                                                                                                                            
 */
ostream& operator << (std::ostream &Strm, const Wektor &Wek){
    Strm<<"(";
    for (int i=0; i<ROZMIAR; i++)
    {
        Strm<<Wek[i]<<" ";
        if(i<(ROZMIAR-1))
        {
            Strm<<", ";
        }
    }
    Strm<<")";
    return Strm;
}

/*!                                                                                                                                                                              
 * Mnozenie wektora przez wektor-iloczyn skalarny                                                                                                                                
 * Argumenty:                                                                                                                                                                    
 * wektor2- wektor, przez ktory mamy pomnozyc inny wektor                                                                                                                        
 * Zwraca:                                                                                                                                                                       
 * iloczyn skalarny wektorow                                                                                                                                                     
 */
double Wektor::operator *(Wektor wektor2) const{
    double iloczyn_skalarny=0;
    for (int i=0; i<ROZMIAR; i++) {
        iloczyn_skalarny = iloczyn_skalarny + wektor[i]*wektor2[i];
    }
    return iloczyn_skalarny;
}

/*!                                                                                                                                                                              
 * Dzielenie wektora przez liczbe                                                                                                                                                
 * Argumenty:                                                                                                                                                                    
 * a- liczba, przez ktora mamy podzielic wektor                                                                                                                                  
 * Zwraca:                                                                                                                                                                       
 * Wynik dzielenia                                                                                                                                                               
 */
Wektor Wektor::operator /(double a) const{
    Wektor nowy_wektor{};
    for (int i=0; i<ROZMIAR; i++) {
        nowy_wektor[i]=wektor[i]/a;
    }
    return nowy_wektor;
}

/*!                                                                                                                                                                              
 * Dodawanie wektorow                                                                                                                                                            
 * Argumenty:                                                                                                                                                                    
 * wektor2- wektor, ktory mamy dodac do innego wektora                                                                                                                           
 * Zwraca:                                                                                                                                                                       
 * sume wektorow                                                                                                                                                                 
 */
Wektor Wektor::operator +(Wektor wektor2) const{
    Wektor nowy_wektor{};
    for (int i=0; i<ROZMIAR; i++) {
        nowy_wektor[i] = wektor[i] + wektor2[i];
    }
    return nowy_wektor;
}

/*!                                                                                                                                                                              
 * Odejmowanie wektorow                                                                                                                                                          
 * Argumenty:                                                                                                                                                                    
 * wektor2- wektor, ktory mamy odjac od innego wektora                                                                                                                           
 * Zwraca:                                                                                                                                                                       
 * roznice wektorow                                                                                                                                                              
 */
Wektor Wektor::operator -(Wektor wektor2) const{
    Wektor nowy_wektor{};
    for (int i=0; i<ROZMIAR; i++) {
        nowy_wektor[i] = wektor[i] - wektor2[i];
    }
    return nowy_wektor;
}
