#include "UkladRownanLiniowych.hh"
#include "Wektor.hh"
#include "Macierz.hh"
#include "rozmiar.hh"

using namespace std;

/*!                                                                                                                                                                              
 * Konstruktor dla klasy Macierz                                                                                                                                                 
 * Inicjuje pola klasy poczatkowymi wartosciami                                                                                                                                  
 */
Macierz::Macierz() {
    for (int i=0; i<ROZMIAR; i++)
    {
        for (int j=0; j<ROZMIAR; j++)
        {
            macierz[i][j]=0;
        }
    }
}

/*!
 * Przeciazenia operatora fukcyjnego dla macierzy
 * Argumenty:
 * wiersz -ilosc wierszy
 * kolumna- ilosc kolumn
 * Zwraca:
 * Macierz o danym rozmiarze
 * lub w przypadku zlego rozmiaru komunikat o bledzie
 */

const double &Macierz::operator()(unsigned int wiersz, unsigned int kolumna) const {
    if (wiersz < 0 || wiersz >= ROZMIAR || kolumna < 0 || kolumna >= ROZMIAR) {
        clog << "Indeks tablicy wyszedl poza zakres" << endl;
        exit(1);
    }
    return macierz[wiersz][kolumna];
}

double &Macierz::operator()(unsigned int wiersz, unsigned int kolumna) {

    return const_cast<double &>(const_cast<const Macierz *>(this)->operator()(wiersz, kolumna));
}

/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wyswietlania macierzy                                                                                                                    
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wyjscia                                                                                                                                                     
 *    Mac - wektor, ktory funkcja ma wczytac,                                                                                                                                    
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu ostream                                                                                                                                            
 */
ostream& operator << (std::ostream &Strm, const Macierz &Mac){

    for (int i=0; i<ROZMIAR; i++)
    {
        for (int j=0; j<ROZMIAR; j++)
        {
            Strm<<Mac(i,j)<<" ";
        }
        Strm<<endl;
    }
    return Strm;
}
/*!                                                                                                                                                                              
 * Realizuje przeciazanie operatora dla wczytywania macierzy                                                                                                                     
 * Argumenty:                                                                                                                                                                    
 *    Strm- strumien wejscia                                                                                                                                                     
 *    Mac - wektor, ktory funkcja ma wczytac,                                                                                                                                    
 *Zwraca:                                                                                                                                                                        
 *      Referencje do obiektu istream                                                                                                                                            
 */
istream& operator >> (std::istream &Strm, Macierz &Mac){

    for (int i=0; i<ROZMIAR; i++)
    {
        for (int j=0; j<ROZMIAR; j++)
        {
            Strm>>Mac(i,j);
        }
    }
    return Strm;
}

/*!
 * Mnozenie macierzy przez wektor
 * Argumenty:
 * vec- wektor, przez ktory mamy pomnozyc macierz
 * Zwraca:
 * Wynik dzialania
 */
Wektor Macierz::operator*(Wektor wektor) const {
   Wektor weektor;
    for (int i = 0; i < ROZMIAR; i++) {
        for (int j = 0; j < ROZMIAR; j++) {
            weektor[i] = weektor[i] + macierz[i][j]*weektor[j];
        }
    }
    return weektor;
}


/*!
* Realizuje zamienianie wierszy macierzy jesli na przekatnej znajduje sie 0
* Argumenty:
*    macierz - macierz, ktora ma byc zmieniona
*    znak-   zmienna potrzebna do zmiany znaku wyznacznika
*/
void Macierz::ZeraNaPrzekatnej(int &znak) {
    //sprawdzenie czy na przekatnej znajduje sie 0
    for (int i = 0; i < ROZMIAR; i++) {
        if (macierz[i][i] == 0) { //jesli tak zamiana wierszy
            for (int a = i; a < ROZMIAR; a++) { //szukamy wiersza z ktorym mozemy zamienic
                if (macierz[a][i] != 0) {
                    znak = (-1)*znak;
                    for (int j = 0; j < ROZMIAR; j++) {
                        swap(macierz[i][j], macierz[a][j]);
                    }
                }
            }
        }
    }
}

/* !
* Realizuje zerowanie wierszy macierzy metoda eliminacji Gaussa
* Argumenty:
*    det-  wyznacznik macierzy
*
*/
void Macierz::ZerowanieWierszy(double &det) {
    double wsp; //zmienna potrzebna przy zerowaniu wierszy
    for (int a = 0; a < ROZMIAR-1; a++) { //dla liczenia stalej wsp=macierz[i][a]/macierz[a][a]
        if (macierz[a][a] != 0) { ///sprawdzenie aby uniknac dzielenia przez 0
            for (int i = a + 1; i < ROZMIAR; i++) { //porusza sie po wierszach w kolumnie do wyzerowania
                wsp = macierz[i][a] / macierz[a][a]; //obliczenie wartosci wspolczynnika do pomnozenia calego wiersza
                for (int j = 0; j < ROZMIAR; j++) { //pomnozenie przez stala  
                    macierz[i][j] = macierz[i][j] - macierz[a][j] * wsp;
                }
            }
        } else det = macierz[a][a];
    }
}


/*!
* Realizuje wyliczenie wyznacznika macierzy
* Argumenty:
*    macierz - macierz, ktorej wyznacznik ma zostac policzony
* Zwraca:
*    det- policzony wyznacznik
*/
double Macierz::WyznacznikMacierzy(Macierz macierz) const {
    double det = 1;
    int znak = 1; //zmienna potrzebna do zmiany znaku jesli zamienimy wiersze
    macierz.ZeraNaPrzekatnej(znak); //sprawdzenie czy na przekatnej nie ma zera
    macierz.ZerowanieWierszy(det); //zerowanie wierszy
    if (det != 0) {
        for (int i = 0; i < ROZMIAR; i++)
            det = det*macierz(i, i)*znak;
    }
    return det;
}

/*!
* Realizuje transpozycje macierzy
*/
void Macierz::Transponowanie() {

    for (int i = 0; i < ROZMIAR; i++) {
        for (int j = i + 1; j < ROZMIAR; j++) {
            swap(macierz[i][j], macierz[j][i]);
        }
    }
}